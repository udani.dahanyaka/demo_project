package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @Value("${message:Hello default}")
    private String message;

    @RequestMapping("/hello")
    String getMessage() {
        return this.message;
    }

    @RequestMapping("/hello-world")
    public String hello_world()
    {
        return "hello world";
    }
}
